# storage/nvdimm/nvdimm_flush_trigger_by_sysfs

Storage: nvdimm feature test for nvdimm_flush trigger by sysfs BZ1457556

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
