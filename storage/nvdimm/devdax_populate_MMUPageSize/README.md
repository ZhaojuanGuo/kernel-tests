# storage/nvdimm/devdax_populate_MMUPageSize

Storage: nvdimm devdax Populate MMUPage Size, BZ1526251

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
